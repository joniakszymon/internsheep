import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';
import JobOffers from './components/JobOffers';
import MainPage from './components/MainPage';

// Import the Auth0 configuration
import { domain, clientId } from "../auth_config.json";

// Import the plugin here
import { Auth0Plugin } from "./auth";

// Install the authentication plugin here
Vue.use(Auth0Plugin, {
  domain,
  clientId,
  onRedirectCallback: appState => {
    router.push(
      appState && appState.targetUrl
        ? appState.targetUrl
        : window.location.pathname
    );
  }
});


Vue.use(VueRouter);

const routes = [
  { path: '/job_offers', name: JobOffers, component: JobOffers },
  { path: '/main_page', name: MainPage, component: MainPage },

];

const router = new VueRouter({
  routes,
  mode: 'history'
});

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  render: h => h(App)
});